#!/bin/sh

dotfile_dir=${PWD#$HOME/}
cd $HOME

for dotfile in $(cat ${dotfile_dir}/.dotfiles); do
	[ -e $dotfile -a "$(realpath $dotfile)" != "$(realpath ${dotfile_dir}/$dotfile)" ] && rm -v $dotfile
	[ -e $dotfile ] || ln -sv $dotfile_dir/$dotfile
done
