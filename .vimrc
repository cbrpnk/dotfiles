set nocompatible
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set number
set clipboard=unnamed

map <F11> :set invpaste<CR>
map <F12> :set invnumber<CR>:set invlist<CR>

syntax enable
set list listchars=tab:\|·
highlight SpecialKey ctermfg=darkgrey
highlight OverLength ctermbg=red
match OverLength /\%81v.\+/
match ErrorMsg /\s\+$/

filetype plugin indent on
au FileType python setl smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
au FileType ruby setl et ts=2 sts=2 sw=2
au FileType yaml setl et ts=2 sts=2 sw=2

if filereadable(expand("~/.vim/autoload/plug.vim"))
    call plug#begin("~/.vim/plugged")
    Plug 'puppetlabs/puppet-syntax-vim', { 'for': 'puppet' }
    Plug 'saltstack/salt-vim', { 'for': 'sls' }
    Plug 'fatih/vim-go', { 'for': 'go' }
    Plug 'b4b4r07/vim-hcl', { 'for': 'hcl' }
    call plug#end()
endif
