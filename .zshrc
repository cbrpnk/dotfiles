### Environment
export PATH=$HOME/.local/bin:$HOME/bin:$PATH
export PAGER='less'
export LESS='-R'
export BLOCKSIZE='K'
export LANG="en_US.UTF-8"
export LC_TIME="en_GB.UTF-8"
export LC_PAPER="en_GB.UTF-8"
export LC_MEASUREMENT="en_GB.UTF-8"


### History settings
setopt append_history
setopt extended_history
setopt hist_allow_clobber
setopt hist_ignore_space
setopt hist_ignore_dups
if [ -z "$SUDO_USER" ]; then
    export HISTFILE=$HOME/.zsh_history
else
    export HISTFILE=$HOME/.zsh_history.$SUDO_USER
fi
export HISTSIZE=1000
export SAVEHIST=1000


### Key bindings
bindkey -e
# xterm sequences
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
# urxvt sequences
bindkey "^[[7~" beginning-of-line
bindkey "^[[8~" end-of-line
# screen/tmux sequences
bindkey "^[[1~" beginning-of-line
bindkey "^[[4~" end-of-line
# del fix
bindkey "^[[3~" delete-char


### Options
setopt extendedglob
setopt no_beep
setopt no_clobber
setopt no_auto_menu
setopt no_auto_remove_slash


### Prompt and VCS Info
function nohist_prompt_hook {
	if [ -z "${HISTFILE}" ]; then
		nohist_prompt_msg_="%F{red}NOHIST%f "
	else
		unset nohist_prompt_msg_
	fi
}
setopt prompt_subst
autoload -Uz vcs_info
autoload -Uz add-zsh-hook
add-zsh-hook precmd vcs_info
add-zsh-hook precmd nohist_prompt_hook
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' formats '%s:%F{magenta}%b%f%c%u '
zstyle ':vcs_info:*' actionformats '%s:%F{magenta%b%f[%F{red}%a%f]%c%u '
zstyle ':vcs_info:*' stagedstr '%F{green}+%f'
zstyle ':vcs_info:*' unstagedstr '%F{red}!%f'

if [ "$TERM_PROGRAM" = "vscode" ]; then
    export PROMPT='${vcs_info_msg_0_}${nohist_prompt_msg_}%(!.%F{red}.%F{grey})%#%f '
else
    export PROMPT='%F{blue}%n%f@%F{green}%m%f %F{yellow}%~%f ${vcs_info_msg_0_}${nohist_prompt_msg_}%(!.%F{red}.%F{grey})%#%f '
fi


### Misc settings
case $TERM in
    xterm*|rxvt*)
        precmd () {print -Pn "\e]0;%m\a"}
    ;;
esac

zstyle :compinstall filename '/home/boogie/.zshrc'

autoload -Uz compinit
compinit

autoload -U select-word-style
select-word-style bash


### Aliases and functions
case `uname` in
    FreeBSD|Darwin)
        alias l='ls -FG'
    ;;
    Linux)
        alias l='ls -F --color=auto'
    ;;
esac

alias ll='l -Ahl'
alias grep='grep --color=auto'
alias gr='grep -Inr --exclude-dir=".*"'
alias i='grep -Fi'
alias diff='diff --color=auto'
alias p='pwd'
alias h='fc -ldD'
alias j='jobs -l'
alias pu='pushd'
alias po='popd'
alias d='dirs'
alias sush='sudo -E -s'
alias sued='sudoedit'
alias zsh-reload='. ~/.zshrc'

### k8s stuff
alias kc=kubectl

kc-ctx() {
    if [ -z "${1}" ]; then
        kubectl config get-contexts
    else
        kubectl config use-context ${1}
    fi
}


### Setting up the correct EDITOR
if [ -x /usr/bin/vimx ]; then
    export EDITOR='vimx'
else
    export EDITOR='vim'
fi

alias vim=$EDITOR

pvim() {
    if [ -z "$TMUX" ]; then
        $EDITOR $*
    else
        local width=$(tmux display -p '#{pane_width}')
        local height=$(tmux display -p '#{pane_height}')
        if [[ $width -ge $((height * 2)) ]]; then
            tmux split-window -h $EDITOR $*
        else
            tmux split-window -v $EDITOR $*
        fi
    fi
}


tmx() {
    cd $HOME && tmux new -AD -s ${1:-main}
}

tmux-env-refresh() {
    for i in DISPLAY SSH_AUTH_SOCK; do
        eval $(tmux show-environment -s DISPLAY)
    done
}

pid () {
    ps aux | grep "${1}" | grep -v "grep ${1}"
}

### Include .zshrc.local if it does exist
[ -r ~/.zshrc.local ] && . ~/.zshrc.local
